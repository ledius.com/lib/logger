import { Inject, Injectable, NestMiddleware } from '@nestjs/common';
import { AsyncLocalStorage } from 'async_hooks';
import {
  ASYNC_STORAGE,
  CORRELATION_ID_HEADER,
  RequestContext,
} from '@ledius/request-context';
import { v4 } from 'uuid';
import { NextFunction, Request, Response } from 'express';
import { LoggerService } from './service/logger.service';

@Injectable()
export class LoggerMiddleware implements NestMiddleware {
  constructor(
    private readonly logger: LoggerService,
    @Inject(ASYNC_STORAGE)
    private readonly asyncLocalStorage: AsyncLocalStorage<RequestContext>,
  ) {}

  public use(req: Request, res: Response, next: NextFunction) {
    const correlationId = req.header(CORRELATION_ID_HEADER) || v4();
    res.setHeader(CORRELATION_ID_HEADER, correlationId);

    this.asyncLocalStorage.run({ correlationId }, () => {
      next();
    });
  }
}
