import {
  CallHandler,
  ExecutionContext,
  Inject,
  Injectable,
  NestInterceptor,
} from '@nestjs/common';
import { Observable, tap } from 'rxjs';
import { LoggerService } from './service/logger.service';
import { AsyncLocalStorage } from 'async_hooks';
import {
  ASYNC_STORAGE,
  CORRELATION_ID_HEADER,
  RequestContext,
} from '@ledius/request-context';
import { Request, Response } from 'express';
import { SKIP_LOGS_METADATA_KEY } from './decorators/skip-logs';

@Injectable()
export class LoggerInterceptor implements NestInterceptor {
  constructor(
    private readonly logger: LoggerService,
    @Inject(ASYNC_STORAGE)
    private readonly asyncLocalStorage: AsyncLocalStorage<RequestContext>,
  ) {}

  public intercept(
    context: ExecutionContext,
    next: CallHandler,
  ): Observable<any> {
    const skipLogs = Reflect.getMetadata(
      SKIP_LOGS_METADATA_KEY,
      context.getHandler(),
    );
    if (skipLogs) {
      return next.handle();
    }

    if (context.getType() !== 'http') {
      return next.handle();
    }

    const response = context.switchToHttp().getResponse<Response>();
    const request = context.switchToHttp().getRequest<Request>();
    const correlationId =
      this.asyncLocalStorage.getStore()?.correlationId || '';

    if (correlationId) {
      response.setHeader(CORRELATION_ID_HEADER, correlationId);
    }

    this.logger.log({
      message: 'Incoming request',
      body: request.body,
      query: request.query,
      path: request.path,
      ip: request.ip,
      headers: request.headers,
    });

    return next.handle().pipe(tap((response) => this.logger.log({ response })));
  }
}
