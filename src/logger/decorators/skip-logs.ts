import { SetMetadata } from '@nestjs/common';

export const SKIP_LOGS_METADATA_KEY = 'LEDIUS_LOGGER_SKIP_LOGS';

export const SkipLogs = () => SetMetadata(SKIP_LOGS_METADATA_KEY, true);
