import {
  Inject,
  Injectable,
  LoggerService as LoggerServiceInterface,
  LogLevel,
} from '@nestjs/common';
import { AsyncLocalStorage } from 'async_hooks';
import { ASYNC_STORAGE, RequestContext } from '@ledius/request-context';

@Injectable()
export class LoggerService implements LoggerServiceInterface {
  private readonly logger: LoggerServiceInterface = console;

  constructor(
    @Inject(ASYNC_STORAGE)
    private readonly asyncLocalStorage: AsyncLocalStorage<RequestContext>,
  ) {}

  private formatMessage(message: any): string {
    return JSON.stringify({
      message,
      correlationId:
        this.asyncLocalStorage.getStore()?.correlationId || 'Unknown',
    });
  }

  public debug(message: any, ...optionalParams: any[]): void {
    this.logger.debug(this.formatMessage(message), ...optionalParams);
  }

  public error(message: any, ...optionalParams: any[]): void {
    this.logger.error(this.formatMessage(message), ...optionalParams);
  }

  public log(message: any, ...optionalParams: any[]): void {
    this.logger.log(this.formatMessage(message), ...optionalParams);
  }

  public setLogLevels(levels: LogLevel[]): void {
    this.logger.setLogLevels(levels);
  }

  public verbose(message: any, ...optionalParams: any[]): void {
    this.logger.verbose(this.formatMessage(message), ...optionalParams);
  }

  public warn(message: any, ...optionalParams: any[]): void {
    this.logger.warn(this.formatMessage(message), ...optionalParams);
  }
}
