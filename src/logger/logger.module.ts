import { MiddlewareConsumer, Module, NestModule } from '@nestjs/common';
import { LoggerService } from './service/logger.service';
import { LoggerMiddleware } from './logger.middleware';
import { APP_INTERCEPTOR } from '@nestjs/core';
import { LoggerInterceptor } from './logger.interceptor';
import { RequestContextModule } from '@ledius/request-context';

@Module({
  imports: [RequestContextModule],
  providers: [
    LoggerService,
    LoggerMiddleware,
    { provide: APP_INTERCEPTOR, useClass: LoggerInterceptor },
  ],
  exports: [LoggerService, LoggerMiddleware],
})
export class LoggerModule implements NestModule {
  configure(consumer: MiddlewareConsumer): any {
    consumer.apply(LoggerMiddleware).forRoutes('');
  }
}
